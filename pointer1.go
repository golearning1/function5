package main

import "fmt"

func main() {
	s := 1
	example(&s)
	fmt.Println(s)

}

func example(p *int) {
	fmt.Println("Son", *p)
	*p = 10

}
