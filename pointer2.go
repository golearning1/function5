package main

import "fmt"

func main() {
	m := "First message "
	message(&m)
	fmt.Println(m)

}

func message(msg *string) {
	*msg = "Second message"

}
