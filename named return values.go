package main

import "fmt"

func main() {

	len, yigindi := sum(12, 23, 45)
	fmt.Println("Uzunlik: ", len, "Yigindi: ", yigindi)
}

func sum(nums ...int) (length, yigindi int) {
	yigindi = 0
	for _, num := range nums {
		yigindi += num
	}
	length = len(nums)
	return
}
